terraform {
  required_version = ">= 1.0.0"

  required_providers {
    kind = {
      source  = "justenwalker/kind"
      version = "0.17.0"
    }
  }
}
