resource "kind_cluster" "my-cluster" {
    name = "kindcluster"
    count = var.create_module ? 1 : 0
    kubeconfig_path = pathexpand(var.kube_config)
    node_image = "kindest/node:v1.24.0"
    wait_for_ready = true
    
    kind_config {
      kind = "Cluster"
      api_version = "kind.x-k8s.io/v1alpha4"

      node {
        role = "control-plane"
      }

      node {
        role = "worker"
      }
    }
  }
  resource "null_resource" "cluster" {
  count = var.create_module && var.set_kubecfg ? 1 : 0


  provisioner "local-exec" {
     command="kind export kubeconfig"
  }
  depends_on = [ kind_cluster.my-cluster ]
}