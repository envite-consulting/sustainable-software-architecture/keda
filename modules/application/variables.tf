variable "create_module" {
  type    = bool
  default = false
}
variable "service_type" {
  type    = string
  default = "NodePort"
}
variable "image_name" {
  type    = string
  default = "17895/myapp:latest"
}
variable "ingress_name" {
  type    = string
  default = "my-app"
}
variable "deployment_name"{
    type=string
    default="my-app"
}
variable "service_name" {
    type = string
    default = "my-app-service"
}
variable "namespace_name" {
    type = string
    default = "default"
}
variable "service_port" {
    type = number
    default = 3000
}
variable "number_replicats" {
    type = number
    default = 0
}
variable "keda_service_name"{
    type=string
    default="keda-add-ons-http-interceptor-proxy"
}