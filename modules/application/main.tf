resource "helm_release" "ingress-controller" {
  name       = "nginx-ingress-controller"
  count      = var.create_module ? 1 : 0
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  set {
    name  = "service.type"
    value = var.service_type
  }

}


resource "kubernetes_deployment" "application" {
  metadata {
    name = var.deployment_name
    labels = {
      app = var.deployment_name
    }
  }

  spec {
    replicas = var.number_replicats

    selector {
      match_labels = {
        app = var.deployment_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.deployment_name
        }

      }

      spec {
        container {
          image = var.image_name
          name  = var.deployment_name
        }
      }
    }
  }
}

resource "kubernetes_service" "applicationService" {
  metadata {
    name = var.service_name
  }
  spec {
    selector = {
      app = var.deployment_name
    }
    session_affinity = "ClientIP"
    port {
      port        = var.service_port
      target_port = var.service_port
    }

    type = "ClusterIP"
  }
    depends_on = [ kubernetes_deployment.application ]

}
resource "kubernetes_ingress_v1" "application_ingress" {
  metadata {
    name = var.ingress_name
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }

  }

  spec {
    rule {
      host = "test"
      http {
        path {
          backend {
                 service {
              name = var.keda_service_name
              port {
                number = 8080
              }
            }
 
          }

          path = "/"
        }
      }
    }
  }
  depends_on = [kubernetes_service.applicationService]
}
