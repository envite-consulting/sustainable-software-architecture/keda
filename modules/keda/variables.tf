variable "create_module" {
  type    = bool
  default = false
}
variable "deployment_name"{
    type=string
    default="my-app"
}
variable "service_name" {
    type = string
    default = "my-app-service"
}
variable "namespace_name" {
    type = string
    default = "default"
}
variable "service_port" {
    type = number
    default = 3000
}
variable "minimum_replicat" {
    type = number
    default = 0
}
variable "maximum_replicat" {
    type = number
    default = 3
}