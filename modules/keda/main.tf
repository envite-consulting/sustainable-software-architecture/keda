resource "helm_release" "keda" {
  name       = "keda"
  count = var.create_module ? 1 : 0
  repository = "https://kedacore.github.io/charts"
  chart      = "keda"
  version    = "2.11.2"

  set {
    name  = "service.type"
    value = "ClusterIP"
  }
}
resource "helm_release" "keda-add-ons-http" {
  name       = "keda-add-on"

  repository = "https://kedacore.github.io/charts"
  chart      = "keda-add-ons-http"
  version = "0.5.3"
  set {
    name  = "service.type"
    value = "ClusterIP"
  }
  depends_on = [ helm_release.keda ]
}


resource "kubernetes_manifest" "scaledObject"{
  manifest = {
    apiVersion = "http.keda.sh/v1alpha1"
    kind       = "HTTPScaledObject"
    metadata = {
      "name"      = "prometheus-scaledobject"
      "namespace" = var.namespace_name
    }
    spec = {
        host = "test"
        targetPendingRequests= 100
        scaleTargetRef = {
            "deployment" = var.deployment_name
            "service" = var.service_name
            "port"= var.service_port
        }
        replicas = {
            "min" = var.minimum_replicat
            "max" = var.maximum_replicat
        }
    }
  }
  depends_on = [ helm_release.keda,helm_release.keda-add-ons-http ]
  
}