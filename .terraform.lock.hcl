# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.4.1"
  constraints = "2.4.1"
  hashes = [
    "h1:EREw6C8BROe6E2ltcs+Kdlq/khJKylq9z25R23NQXUM=",
    "zh:07517b24ea2ce4a1d3be3b88c3efc7fb452cd97aea8fac93ca37a08a8ec06e14",
    "zh:11ef6118ed03a1b40ff66adfe21b8707ece0568dae1347ddfbcff8452c0655d5",
    "zh:1ae07e9cc6b088a6a68421642c05e2fa7d00ed03e9401e78c258cf22a239f526",
    "zh:1c5b4cd44033a0d7bf7546df930c55aa41db27b70b3bca6d145faf9b9a2da772",
    "zh:256413132110ddcb0c3ea17c7b01123ad2d5b70565848a77c5ccc22a3f32b0dd",
    "zh:4ab46fd9aadddef26604382bc9b49100586647e63ef6384e0c0c3f010ff2f66e",
    "zh:5a35d23a9f08c36fceda3cef7ce2c7dc5eca32e5f36494de695e09a5007122f0",
    "zh:8e9823a1e5b985b63fe283b755a821e5011a58112447d42fb969c7258ed57ed3",
    "zh:8f79722eba9bf77d341edf48a1fd51a52d93ec31d9cac9ba8498a3a061ea4a7f",
    "zh:b2ea782848b10a343f586ba8ee0cf4d7ff65aa2d4b144eea5bbd8f9801b54c67",
    "zh:e72d1ccf8a75d8e8456c6bb4d843fd4deb0e962ad8f167fa84cf17f12c12304e",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.23.0"
  constraints = "2.23.0"
  hashes = [
    "h1:S0dS3oy5c6ma2JUzpbtO45wb5iSCJdFFiUDf/t99tws=",
    "zh:10488a12525ed674359585f83e3ee5e74818b5c98e033798351678b21b2f7d89",
    "zh:1102ba5ca1a595f880e67102bbf999cc8b60203272a078a5b1e896d173f3f34b",
    "zh:1347cf958ed3f3f80b3c7b3e23ddda3d6c6573a81847a8ee92b7df231c238bf6",
    "zh:2cb18e9f5156bc1b1ee6bc580a709f7c2737d142722948f4a6c3c8efe757fa8d",
    "zh:5506aa6f28dcca2a265ccf8e34478b5ec2cb43b867fe6d93b0158f01590fdadd",
    "zh:6217a20686b631b1dcb448ee4bc795747ebc61b56fbe97a1ad51f375ebb0d996",
    "zh:8accf916c00579c22806cb771e8909b349ffb7eb29d9c5468d0a3f3166c7a84a",
    "zh:9379b0b54a0fa030b19c7b9356708ec8489e194c3b5e978df2d31368563308e5",
    "zh:aa99c580890691036c2931841e88e7ee80d59ae52289c8c2c28ea0ac23e31520",
    "zh:c57376d169875990ac68664d227fb69cd0037b92d0eba6921d757c3fd1879080",
    "zh:e6068e3f94f6943b5586557b73f109debe19d1a75ca9273a681d22d1ce066579",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/justenwalker/kind" {
  version     = "0.17.0"
  constraints = "0.17.0"
  hashes = [
    "h1:fQ95Tr9sxiSCCZk1jAYV3KrEnLn4jd5/L8IC9nvn1+8=",
    "zh:09717f35c8b18d6aae5de21cc72421e9903337cd069da3b6619b290615ff114a",
    "zh:0e2efabd4f55b5973cd3a84367ef85dac4652ef4c7848cac0eedd477748ae709",
    "zh:1cee919ae076f3aabd6e034f065097cf9b6549a7b8cc35a99b6e611edd553ec5",
    "zh:20f7d59d37908d5dfa0587a9c991b51085ef8fc82b6a292079882d12942fd737",
    "zh:41306499d5688bbcdb6551257ced0bae961ed10fc9916beb6ac31ab6f37cd21f",
    "zh:51f8b9d72a513bdeeaf78484ea5fb446bed01c6dc54c44045871e874e30e4c6d",
    "zh:53e0a5b1c499b65448021308e9d275ac695ac6959110ed0f85d7aa3273122b5b",
    "zh:69d016cb99cab6485c1ce09a5d352d2f9df9e22d9cdc097bd9c9eb1de6930852",
    "zh:6a45910d3473a75fb8c9d2ed764181a4d2d4a93e8a6a4b1eacd434f600970f4e",
    "zh:c1396de33497f594c0f2411b6cfde0b984dc81fa7b5bad66cdd666306da6cb2a",
    "zh:d7cd47d2770cd080f6009783183e44e762af887b5262c5bbb9c8b47056b7d2f1",
    "zh:dc0910eeec21dc6a4f06f47c8b48b445b1f4a14a6ffb1c607c8ed972296e971f",
    "zh:e3be2ab08055529a5e98165edc1ee2450df0f237ce7f2d5e23cf051953e33e5f",
    "zh:e86f5688375051dddbd5b9b336aa95408e4f35e7e058ecd0cd746e35e7c7a073",
  ]
}
